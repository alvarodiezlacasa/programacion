package main;
import java.util.Scanner;
public class main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		
		int quequieres;
		double area;
		double PI;
		double volumen;
		double radio;
		
		System.out.println("**Men� principal**");
		System.out.println("�Qu� quieres saber?");
		System.out.println("1: �rea del c�rculo");
		System.out.println("2: Volumen de la esfera");
			quequieres=in.nextInt();
			
			if(quequieres == 1){
				System.out.println("**�REA DEL C�RCULO**");
				
				System.out.println("�Cu�l es el radio del c�rculo?");
				radio=in.nextDouble();
				PI=3.1416;
				area=PI*(radio*radio);
				System.out.println("El �rea del c�rculo es " + area);
			}
			
			if(quequieres == 2){
				System.out.println("**VOLUMEN DE LA ESFERA**");
				
				System.out.println("�Cu�l es el radio de la esfera?");
				radio=in.nextDouble();
				PI=3.1416;
				volumen=4*PI*(radio*radio*radio)/3;
				System.out.println("El volumen de la esfera es " + volumen + " metros c�bicos");
			}
			if(quequieres<1 || quequieres>2){
				System.out.println("**ERROR**");
			}
		
	}

}
