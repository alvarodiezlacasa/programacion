package main;

			//1� importar Scanner

import java.util.Scanner;
public class main {

	public static void main(String[] args){
		
			//2� Declarar el "teclado"
		
		Scanner in= new Scanner(System.in);
		
		//3� Leer un byte
		
		System.out.println("�Qu� edad tienes?");
		byte edad;
		edad=in.nextByte();
		System.out.println("Tengo " + edad + " a�os");
		
			//4� Leer un int
		
		System.out.println("Qu� sueldo tienes?");
		int sueldo;
		sueldo=in.nextInt();
		System.out.println("Mi sueldo es de "+ sueldo + "� al mes");
		
			//5� Leer un float
		
		System.out.println("Qu� nota has sacado?");
		float nota;
		nota=in.nextFloat(); //4,6
		System.out.println("Tu nota real es: " + nota); //4,6
		System.out.println("Tu nota redondeada es: " + Math.round(nota)); //5
		int notaTruncada;
		notaTruncada=(int)nota; //4
		System.out.println("Tu nota truncada es: " + notaTruncada);
		
			//6� Limpiar el buffer
		
		in.nextLine();
		
			//7� Leer una cadena
		
		System.out.println("�C�mo te llamas?");
		String nombre=in.nextLine();
		System.out.println("Me llamo " + nombre);
	
		
		
		in.close();
		
		
		
	}
}
