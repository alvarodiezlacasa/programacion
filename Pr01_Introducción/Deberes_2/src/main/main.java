package main;
import java.util.Scanner;

public class main {
	
	
	public static void main(String[] args){
	
				//Calcula el menor de 4 n�meros
		
		Scanner in= new Scanner(System.in);
		
		int num1;
		int num2;
		int num3;
		int num4;
		int min;
		int min2;
		int min3;
		
		System.out.println("**MENOR DE 4 N�MEROS**");
		
		System.out.println("�Cu�l es el primer n�mero?");
		num1=in.nextInt();
		System.out.println("�Cu�l es el segundo n�mero?");
		num2=in.nextInt();
		System.out.println("�Cu�l es el tercer n�mero?");
		num3=in.nextInt();
		System.out.println("�Cu�l es el cuarto n�mero?");
		num4=in.nextInt();
		min=Math.min(num1, num2);
		min2=Math.min(num3, num4);
		min3=Math.min(min, min2);
		System.out.println("El menor entre " + num1 + ", " + num2 + ", " + num3 + " y " + num4 + " es: " + min3);
		
	}
}
