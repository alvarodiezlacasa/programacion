package main;

public class main {
	/* 
	 * Tipos de datos: crear variables y asignar datos
	 */
	public static void main(String[] args) {

	//Tipos alfabéticos
	String nombre ="Pepe";
	System.out.println("Hola " + nombre);
	
	
	char estadoCivil; //s-Soltero, c-Casado
	estadoCivil= 's';
	System.out.println("Estado Civil = " + estadoCivil);
	
	//Tipos de datos numéricos
	
	byte edad;
	edad= 23;
	System.out.println("Edad\t = " + edad);
	
	short sueldo=12345;
	System.out.println("El sueldo\t= " + sueldo);
	
	int sueldoAnual;
	sueldoAnual=sueldo*12;
	System.out.println("Sueldo anual " + sueldoAnual);
	
	long sueldoAnualIncrementado;
	sueldoAnualIncrementado=sueldoAnual + sueldoAnual*50/100;
	//2 opciones para ponerlo
	System.out.println("Sueldo anual con incremento de 50% = " 
					+ sueldoAnualIncrementado);
	System.out.println("Sueldo anual con incremento de 50% = " 
					+ (sueldoAnual + sueldoAnual*50/100));
	 
	
	}
}