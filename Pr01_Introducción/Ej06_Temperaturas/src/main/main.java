package main;

	/*
	 * Realizar un programa que calcule ap artir de una 
	 * temperatura expresada en grados centígrados(C), 
	 * sus equivalentes en las escalas de Reamhur (R), Farenheit (F) y Kelvin (K).
	 * 
	 */

public class main {

		public static void main(String[] args) {
			double centigrados=23.5;
			System.out.println("Grados centígrados = " + centigrados);
			
			double reamhur=centigrados*0.8;
			System.out.println("En Reamhur = " + reamhur);
			
			double faren=centigrados*1.8+32;
			System.out.println("En Farenheit = " + faren);
			
			double kelvin=centigrados+273;
			System.out.println("En Kelvin = " + kelvin);
		}
}
