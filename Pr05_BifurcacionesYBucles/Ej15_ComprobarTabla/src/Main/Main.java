package Main;
import java.util.Scanner;
public class Main {

	public static void main(String[] args){
		Scanner in=new Scanner(System.in);
		
		int tabla;
		int numTablas;
		int resultado;
		int contadorAciertos=0;			//	System.out.println("Has tenido " +contadorAciertos+ " aciertos");System.out.println("Has tenido " +contadorFallos+ " fallos");
		int contadorFallos=0;
		int contadorTablas=0;
		
		
		System.out.println("Cuantas tablas quieres hacer");
		numTablas=in.nextInt();
		
do{		
		do {
			System.out.println("�Qu� tabla quieres?");
			tabla=in.nextInt();
			
			if (tabla < 1 || tabla >10){
				System.out.println("**ERROR**");
			}
			
		}while (tabla<1 || tabla>10);
		
		for (int n=1; n<=10; n++) {
			
			System.out.println(tabla + " * " + n + " = ");
			resultado=in.nextInt();
			
			if (resultado==(tabla*n)){
				System.out.println("Bien");
				contadorAciertos=contadorAciertos+1;
			}
			else {
				System.out.println("Mal. El resultado es " + (tabla*n));
				contadorFallos=contadorFallos+1;
			}
		}	
		
		contadorTablas++;
}while (contadorTablas<numTablas);

System.out.println("Has tenido " +contadorAciertos+ " aciertos");
System.out.println("Has tenido " +contadorFallos+ " fallos");
		
		if (contadorTablas==numTablas){
			System.exit(0);
		}
		
		
	}
}
