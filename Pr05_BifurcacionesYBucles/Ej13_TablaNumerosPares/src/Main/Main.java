package Main;
import java.util.Scanner;
public class Main {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("***Tabla de multiplicar de tres numeros introducidos por teclado***");

		int numero;
		System.out.println("�Desde que valor hasta el uno quieres las tablas?");

		numero=in.nextInt();
		//comprobar que le numero es par
		
		if(numero%2!=0){numero--;
}
		
		//pintar las tablas pares en decreciente
		
		for(int tabla=numero; tabla>=1; tabla-=2){
			System.out.println("Tabla del " + tabla);


			for( int n=1; n<=10; n++){
				System.out.println(tabla + " x " + n + " = " + (tabla*n));
			}

		}


	}

}