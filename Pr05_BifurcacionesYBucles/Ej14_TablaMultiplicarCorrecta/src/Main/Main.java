package Main;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		
		int tabla;
		int resultado;
		
		// Pedir un numero del 1 al 10
		
		do{
			System.out.println("�Qu� tabla quieres aprender? (1-10)");
			tabla=in.nextInt();
				if (tabla<1 || tabla>10){
					System.out.println("**ERROR**");
				}
		}while (tabla<1 || tabla>10);
		
		//Pintar la tabla y evaluar el resultado
		
		for (int n=1; n<=10; n++) {
			System.out.println(tabla + " * " + n + " = ");
			resultado=in.nextInt();
			
			if (resultado==(tabla*n)) {
				System.out.println("BIEN");
			}
			else{
				System.out.println("MAL. El valor correcto es " + (tabla*n));
			}
		}		
		
	}

}
