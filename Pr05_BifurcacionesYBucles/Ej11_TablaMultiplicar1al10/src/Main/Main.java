package Main;

public class Main {

	public static void main(String[] args) {
		
		System.out.println("***Tabla de multiplicar de del 1 hasta el 10***");
		
		for(int contador=1; contador<=10; contador++){
			System.out.println("Tabla del "+ contador + " : ");

			int tabla=contador;
			for(int n=1; n<=10; n++){
				System.out.println(tabla + " x " + n + " = " + (tabla*n));

				}
		}
	}	
}